package com.senselessweb.alexa.dockercontrol.intent;

import java.util.Locale;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;

import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.senselessweb.alexa.dockercontrol.domain.ContainerInfo;
import com.senselessweb.alexa.dockercontrol.domain.Info;
import com.senselessweb.alexa.dockercontrol.service.DockerControlService;

public class IntentRegistryTest {

	private IntentRegistry testee;
	private ResourceBundleMessageSource messageSource;
	private DockerControlService docker;

	@Before
	public void setup() {
		this.messageSource = new ResourceBundleMessageSource();
		this.messageSource.setBasename("i18n/messages");

		this.docker = Mockito.mock(DockerControlService.class);
		this.testee = new IntentRegistry(docker, new Messaging());
	}

	@Test
	public void testInfo() {
		Mockito.when(this.docker.getInfo()).thenReturn(new Info(2, 3, 4));
		assertText(testee.getInfo(), "intent.info.message", 2, 3, 4);
		assertText(testee.getIntent(InfoIntent.NAME).apply(null), "intent.info.message", 2, 3, 4);
	}

	@Test
	public void testContainerInfoOfRunningContainer() {
		final String name = "myRunningContainer";
		final IntentRequest req = intentRequestWithSlot(ContainerInfoIntent.NAME, ContainerInfoIntent.SLOT_CONTAINERNAME, name);
		
		Mockito.when(this.docker.getContainerInfo(name)).thenReturn(new ContainerInfo(new ContainerInfo.State("running")));
		assertText(testee.getIntent(ContainerInfoIntent.NAME).apply(req), "intent.containerinfo.online.message", name);
	}

	@Test
	public void testContainerInfoOfStoppedContainer() {
		final String name = "myStoppedContainer";
		final IntentRequest req = intentRequestWithSlot(ContainerInfoIntent.NAME, ContainerInfoIntent.SLOT_CONTAINERNAME, name);
		
		Mockito.when(this.docker.getContainerInfo(name)).thenReturn(new ContainerInfo(new ContainerInfo.State("paused")));
		assertText(testee.getIntent(ContainerInfoIntent.NAME).apply(req), "intent.containerinfo.offline.message", name);
	}

	@Test
	public void testContainerInfoWhenNoNameIsGiven() {
		final String name = "myRunningContainer";

		final IntentRequest req = IntentRequest.builder().withRequestId(UUID.randomUUID().toString())
				.withIntent(Intent.builder().withName(name).build()).build();
		assertText(testee.getIntent(ContainerInfoIntent.NAME).apply(req), "intent.containerinfo.error.nocontainername");
	}

	private IntentRequest intentRequestWithSlot(final String name, final String slotName, final String slotValue) {
		final Slot slot = Slot.builder().withName(slotName).withValue(slotValue).build();
		final Intent intent = Intent.builder().withName(name).withSlot(slot).build();
		final IntentRequest req = IntentRequest.builder().withRequestId(UUID.randomUUID().toString()).withIntent(intent).build();
		return req;
	}

	private void assertText(SpeechletResponse response, String code, Object... args) {
		final String expected = messageSource.getMessage(code, args, Locale.GERMAN);
		final PlainTextOutputSpeech speech = (PlainTextOutputSpeech) response.getOutputSpeech();
		Assert.assertEquals(expected, speech.getText());
		Assert.assertFalse(StringUtils.isBlank(speech.getText()));
		for (Object arg : args) {
			Assert.assertTrue(speech.getText().contains(arg.toString()));
		}
	}

}
