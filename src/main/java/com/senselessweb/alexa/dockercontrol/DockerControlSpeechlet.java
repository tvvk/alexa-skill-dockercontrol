package com.senselessweb.alexa.dockercontrol;

import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;
import com.senselessweb.alexa.dockercontrol.intent.IntentRegistry;
import com.senselessweb.alexa.dockercontrol.intent.Messaging;

public class DockerControlSpeechlet implements SpeechletV2 {

	private static final Logger log = LoggerFactory.getLogger(DockerControlSpeechlet.class);

	private final Messaging messaging = new Messaging();
	private final IntentRegistry intents = new IntentRegistry(messaging);

	public void onSessionStarted(SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
		log.info("onSessionStarted requestId={}, sessionId={}", requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
		log.info("Service created");
	}

	public void onSessionEnded(SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
		log.info("onSessionEnded requestId={}, sessionId={}", requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

	public SpeechletResponse onLaunch(SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
		log.info("onLaunchEvent");
		final SpeechletResponse info = intents.getInfo();
		final String code = "launch.greeting." + (info != null ? "online" : "offline");
		return messaging.buildResponse(code);
		
	}

	public SpeechletResponse onIntent(SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
		
		final Intent intent = requestEnvelope.getRequest().getIntent();
		log.info("onIntent: " + intent.getName());
		intent.getSlots().entrySet().stream().forEach(e -> log.info("Slot: " + e.getKey() + ":" + e.getValue().getValue()));
		
		final Function<IntentRequest, SpeechletResponse> theIntent = intents.getIntent(intent.getName());
		if (theIntent == null) 
			return messaging.buildResponse("error.couldnotunderstand");
			
		return theIntent.apply(requestEnvelope.getRequest());
	}
}
