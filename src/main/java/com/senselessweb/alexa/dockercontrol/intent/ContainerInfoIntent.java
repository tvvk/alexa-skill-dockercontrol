package com.senselessweb.alexa.dockercontrol.intent;

import java.util.Optional;

import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.senselessweb.alexa.dockercontrol.domain.ContainerInfo;

public class ContainerInfoIntent extends AbstractIntent {

	static final String NAME = "container_info";
	static final String SLOT_CONTAINERNAME = "containername";

	@Override
	public SpeechletResponse apply(IntentRequest request) {

		final Optional<String> containerName = getSlotValue(request, SLOT_CONTAINERNAME);
		if (!containerName.isPresent()) 
			return getMessaging().buildResponse("intent.containerinfo.error.nocontainername");

		try {
			final ContainerInfo info = getDocker().getContainerInfo(containerName.get());
			final String code = "intent.containerinfo." + (info.isRunning() ? "online" : "offline") + ".message";
			return getMessaging().buildResponse(code, containerName.get());
		} catch (final Exception e) {
			return getMessaging().buildResponse("intent.containerinfo.error.endpointerror", containerName.get());
		}
	}

	@Override
	public String getName() {
		return NAME;
	}

}
