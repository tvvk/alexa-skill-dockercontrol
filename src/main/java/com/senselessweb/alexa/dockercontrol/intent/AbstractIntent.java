package com.senselessweb.alexa.dockercontrol.intent;

import java.util.Optional;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;

import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.senselessweb.alexa.dockercontrol.service.DockerControlService;

public abstract class AbstractIntent implements Function<IntentRequest, SpeechletResponse> {

	private DockerControlService dockerControlService;
	private Messaging messaging;

	public void setDockerControlService(DockerControlService dockerControlService) {
		this.dockerControlService = dockerControlService;
	}

	public DockerControlService getDocker() {
		return dockerControlService;
	}

	public void setMessageSource(Messaging messaging) {
		this.messaging = messaging;
	}

	public Messaging getMessaging() {
		return messaging;
	}

	protected Optional<String> getSlotValue(IntentRequest request, String slotName) {
		final Slot slot = request.getIntent().getSlot(slotName);
		if (slot == null || StringUtils.isBlank(slot.getValue())) {
			return Optional.empty();
		}
		return Optional.of(slot.getValue());
	}

	public abstract String getName();
}
