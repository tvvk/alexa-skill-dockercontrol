package com.senselessweb.alexa.dockercontrol.intent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.senselessweb.alexa.dockercontrol.service.DockerControlService;

public class IntentRegistry {

	private final Messaging messaging;
	private final DockerControlService dockerControlService;
	private final Map<String, Function<IntentRequest, SpeechletResponse>> intents;
	
	public IntentRegistry(final Messaging messaging) {
		this(new DockerControlService(), messaging);
	}
	
	IntentRegistry(final DockerControlService dockerControlService, final Messaging messaging) {
		
		this.messaging = messaging;
		this.dockerControlService = dockerControlService;
		
		this.intents = new HashMap<>();
		this.registerIntent(new InfoIntent());
		this.registerIntent(new ContainerInfoIntent());
		
	}
	
	private void registerIntent(AbstractIntent intent) {
		intent.setDockerControlService(dockerControlService);
		intent.setMessageSource(messaging);
		this.intents.put(intent.getName(), intent);
	}

	public SpeechletResponse getInfo() {
		return intents.get(InfoIntent.NAME).apply(null);
	}

	public Function<IntentRequest, SpeechletResponse> getIntent(String name) {
		Function<IntentRequest, SpeechletResponse> intent = intents.get(name);
		if (intent == null) {
			throw new IllegalArgumentException("There is no such intent: " + name);
		}
		return intent;
	}
}
