package com.senselessweb.alexa.dockercontrol.intent;

import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.senselessweb.alexa.dockercontrol.domain.Info;

public class InfoIntent extends AbstractIntent {
	

	static final String NAME = "info";

	@Override
	public SpeechletResponse apply(IntentRequest req) {
		Info info = getDocker().getInfo();
		return getMessaging().buildResponse("intent.info.message", info.getContainersRunning(), info.getContainersStopped(), info.getContainersPaused());
	}
	
	@Override
	public String getName() {
		return NAME;
	}
}
