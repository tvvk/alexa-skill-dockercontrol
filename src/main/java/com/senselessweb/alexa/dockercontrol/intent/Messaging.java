package com.senselessweb.alexa.dockercontrol.intent;

import java.util.Locale;

import org.springframework.context.support.ResourceBundleMessageSource;

import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.ui.PlainTextOutputSpeech;

public class Messaging {

	private final ResourceBundleMessageSource messageSource;

	public Messaging() {
		this.messageSource = new ResourceBundleMessageSource();
		this.messageSource.setBasename("i18n/messages");
	}

	public String getMessage(String code, Object... args) {
		return messageSource.getMessage(code, args, Locale.GERMAN);
	}

	public SpeechletResponse buildResponse(String code, Object... args) {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(getMessage(code, args));
		SpeechletResponse response = SpeechletResponse.newTellResponse(speech);
		response.setNullableShouldEndSession(Boolean.FALSE);
		return response;
	}
}
