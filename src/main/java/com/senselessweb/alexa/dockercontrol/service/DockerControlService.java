package com.senselessweb.alexa.dockercontrol.service;

import java.util.Collections;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;

import com.senselessweb.alexa.dockercontrol.domain.ContainerInfo;
import com.senselessweb.alexa.dockercontrol.domain.Info;

public class DockerControlService {

	private static final String ENV_BASE_URL = "URL";
	private final RestTemplate restTemplate;
	private final String baseUrl;
	
	public DockerControlService() {
		
		final String baseUrl = System.getenv(ENV_BASE_URL);
		if (StringUtils.isBlank(baseUrl)) {
			throw new IllegalArgumentException("URL not set");
		}
		this.baseUrl = StringUtils.endsWith(baseUrl, "/") ? baseUrl : baseUrl + "/";
		
		this.restTemplate = new RestTemplate();
		this.restTemplate.setInterceptors(
				Collections.<ClientHttpRequestInterceptor>singletonList(new BasicAuthRequestInterceptor()));
	}
	
	public Info getInfo() {
		return restTemplate.getForObject(baseUrl + "info", Info.class);
	}

	public ContainerInfo getContainerInfo(String containerName) {
		if (StringUtils.isBlank(containerName))
			throw new IllegalArgumentException("containerName must not be blank");
		return restTemplate.getForObject(baseUrl + "containers/" + containerName + "/json", ContainerInfo.class);
	}
	
}
