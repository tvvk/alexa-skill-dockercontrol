package com.senselessweb.alexa.dockercontrol.service;

import java.io.IOException;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class BasicAuthRequestInterceptor implements ClientHttpRequestInterceptor {
	
	private static final String ENV_USERNAME = "USERNAME";
	private static final String ENV_PASSWORD = "PASSWORD";
	
	private String basicAuth;

	public BasicAuthRequestInterceptor() {
		final String username = System.getenv(ENV_USERNAME);
		final String password = System.getenv(ENV_PASSWORD);
		
		if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			throw new IllegalArgumentException("Either username or password is not set");
		}
		
		this.basicAuth = "Basic " + new String(Base64.getEncoder().encode((username + ":" + password).getBytes()));
	}

	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {
		request.getHeaders().set("Authorization", basicAuth);
		return execution.execute(request, body);
	}

}
