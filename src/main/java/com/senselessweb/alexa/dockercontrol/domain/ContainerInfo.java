package com.senselessweb.alexa.dockercontrol.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContainerInfo {
	
	private State state;

	public ContainerInfo(
			final @JsonProperty("State") State state) {
		this.state = state;
	}
	
	public static class State {
		
		private String status;

		public State(
				final @JsonProperty("Status") String status) {
			this.status = status;
		}
	}
	
	public boolean isRunning() {
		return "running".equalsIgnoreCase(this.state.status);
	}
}
