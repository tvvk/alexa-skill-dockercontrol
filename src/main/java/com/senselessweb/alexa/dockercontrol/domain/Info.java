package com.senselessweb.alexa.dockercontrol.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Info {
	
	private final int containersRunning;
	private final int containersStopped;
	private final int containersPaused;

	public Info(
			@JsonProperty("ContainersRunning") final int containersRunning,
			@JsonProperty("ContainersStopped") final int containersStopped,
			@JsonProperty("ContainersPaused") final int containersPaused) {
		
		this.containersRunning = containersRunning;
		this.containersPaused = containersPaused;
		this.containersStopped = containersStopped;
	}
	
	public int getContainersRunning() {
		return containersRunning;
	}
	
	public int getContainersStopped() {
		return containersStopped;
	}
	
	public int getContainersPaused() {
		return containersPaused;
	}

}
