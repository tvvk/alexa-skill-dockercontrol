package com.senselessweb.alexa.dockercontrol;

import java.util.HashSet;
import java.util.Set;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

public final class DockerControlSpeechletRequestStreamHandler extends SpeechletRequestStreamHandler {
    private static final Set<String> supportedApplicationIds;
    static {

        supportedApplicationIds = new HashSet<String>();
        supportedApplicationIds.add("amzn1.ask.skill.6414b9ea-88eb-4114-9ce1-79db4b386ccd");
    }

    public DockerControlSpeechletRequestStreamHandler() {
        super(new DockerControlSpeechlet(), supportedApplicationIds);
    }
}
